<?php
/**
 * Created by PhpStorm.
 * User: Alina
 * Date: 23/02/2017
 * Time: 00:30
 */

use Illuminate\Database\Eloquent\Model as Eloquent;

class ville_france extends Eloquent
{
    protected $table = 'villes_france_free';
    protected $fillable = ['ville_nom','ville_code_postal'];

}