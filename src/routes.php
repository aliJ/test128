<?php
// Routes

$app->get('/test128', function ($request, $response, $args) {
    $this->db; //connection à la base de donnée
    $codePostal = null;
    $ville = $request->getQueryParam('ville'); //je récupère la ville saisie
    if($ville != null){
        $villeFr  = ville_france::where("ville_nom","=", $ville)->first();
        if($villeFr == null){
            $codePostal = 'La ville saisie n\'existe pas dans notre base';
        }else {
            $codePostal = $villeFr->ville_code_postal;
        }
    }
    // Render index view
    $this->renderer->render($response, 'index.phtml', ['villeSaisie' => $ville, 'codePostal' => $codePostal]);
    $this->db->getConnection()->disconnect(); //déconnection de la base de données
    return $response;
});
